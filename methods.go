package main

import "encoding/json"

type Njalla struct {
	JsonRPC string       `json:"jsonrpc"`
	Method  string       `json:"method"`
	Params  methodParams `json:"params"`
	//ID      string    `json:"id"`
}

type methodParams interface {
	getJson() ([]byte, error)
}

type addRecord struct {
	Domain  string `json:"domain"`
	ID      int    `json:"id"`
	Content string `json:"content"`
}

func (a addRecord) getJson() ([]byte, error) {
	b, err := json.Marshal(a)
	if err != nil {
		return b, err
	}
	return b, nil
}

type listRecords struct {
	Domain string `json:"domain"`
}

func (l listRecords) getJson() ([]byte, error) {
	b, err := json.Marshal(l)
	if err != nil {
		return b, err
	}
	return b, nil
}
