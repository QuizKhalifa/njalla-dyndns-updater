BINARY_NAME=dipd

all: arm

arm:
	env GOOS=linux GOARCH=arm GOARM=5 go build -o ${BINARY_NAME} main.go methods.go records.go config.go

build:
	go build -o ${BINARY_NAME} main.go methods.go records.go config.go

run:
	go run main.go methods.go records.go config.go

clean:
	go clean
	rm ${BINARY_NAME}