package main

import (
	"bufio"
	"bytes"
	"context"
	"encoding/json"
	"log"
	"net"
	"net/http"
	"os"
	"strings"
	"time"
)

func main() {
	log.Println("Starting dynamic IP defender service...")

	// Load the configuration
	var err error
	CFG, err = loadConfig()
	if err != nil {
		log.Panic(err)
	}
	log.Printf("Config file is loaded\n")

	log.Printf("Init finished. Checking validity once every 5 minutes...\n")
	// While we are running, get our ip and check if it changes
	for {
		// get the current IPs for the DNS' we are monitoring (in case they change form the outside)
		dnsIPs := getDNSIPs()

		// Get our ip
		ip := getOurIP()

		for _, entry := range dnsIPs {

			if entry.IP.String() != ip.String() { // If it isn't the same, we should update it
				log.Printf("Current IP for %s does not match our IP\n", entry.Domain)
				log.Printf("Updating DNS... (old: %s, new: %s)\n", entry.IP, ip)

				_, err := updateDNS(entry.TLD, ip.String(), entry.ID)
				if err != nil {
					log.Println("Failed to update DNS for ", entry.Domain)
				} else {
					log.Println("Successfully updates DNS for ", entry.Domain)
				}

			}

		}

		time.Sleep(300 * time.Second)
	}
}

type dnsDomain struct {
	TLD    string
	Domain string
	ID     int
	IP     net.IP
}

func getDNSIPs() []dnsDomain {

	var domains []dnsDomain = []dnsDomain{}

	// Go through all "tld"s
	for _, tld := range CFG.Domains {
		record := getDomainRecords(tld.TLD) // Get all records for that domain
		if record == nil {
			continue
		}

		for _, subdomain := range tld.Subdomains { // Go through the subdomains
			for _, entry := range record.Result.Records {
				if entry.Name == subdomain { // Find an entry with the same name as the one that is to be monitored
					domains = append(domains, dnsDomain{ // Build a list from that
						TLD:    tld.TLD,
						Domain: entry.Name,
						ID:     entry.ID,
						IP:     net.ParseIP(entry.Content),
					})
				}
			}
		}
	}

	return domains
}

func njalla(method string, methodParams methodParams) ([]byte, error) {
	// Marshal json request
	b, err := json.Marshal(
		Njalla{
			JsonRPC: "2.0",
			Method:  method,
			Params:  methodParams,
		})
	if err != nil {
		log.Println("Failed to json-marshal request to Njalla: " + err.Error())
		return nil, err
	}

	// Build request
	req, err := http.NewRequest("POST", URL, bytes.NewReader(b))
	if err != nil {
		log.Println("Failed to create request to Njalla: " + err.Error())
		return nil, err
	}
	defer req.Body.Close()

	req.Header.Set("Accept", "application/json")
	req.Header.Set("Content-Typ", "application/json")
	req.Header.Set("Authorization", "Njalla "+CFG.Token)

	// Send request
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	resp, err := http.DefaultClient.Do(req.WithContext(ctx))
	if err != nil {
		log.Println("Failed to send request to Njalla: " + err.Error())
		return nil, err
	}
	defer resp.Body.Close()

	// Get response
	buf := make([]byte, 1024)
	resp.Body.Read(buf)

	return buf, nil
}

func updateDNS(domain, newIP string, id int) (string, error) {
	resp, err := njalla("edit-record", addRecord{
		Domain:  domain,
		ID:      id,
		Content: newIP,
	})

	if err != nil {
		return "", err
	}

	// Unmarshal response
	var container map[string]interface{}
	getJSON(resp, &container)
	if val, exists := container["error"]; exists {
		log.Println("Error response from Njalla: ", val)
	}

	return "", nil
}

func getOurIP() net.IP {
	// Get our IP
	resp, err := http.Get("https://ipv4.icanhazip.com")
	if err != nil {
		log.Println("Failed getting our IP")
	}

	defer resp.Body.Close()

	buf := make([]byte, 15) // IP's are maximum 15 characters.
	resp.Body.Read(buf)
	strippedIP := strings.ReplaceAll(string(buf), "\n", "") // The website gives two lines
	strippedIP = strings.ReplaceAll(strippedIP, "\x00", "") // A NULL byte is appended for some reason
	ip := net.ParseIP(strippedIP)
	if ip == nil {
		log.Println("Failed parsing IP")
	}

	return ip
}

func getDomainRecords(domain string) *records {
	resp, err := njalla("list-records", listRecords{
		Domain: domain,
	})
	if err != nil {
		log.Println("Couldn't list records: ", err)
		return nil
	}
	DNSRecords := &records{}
	getJSON(resp, DNSRecords)

	return DNSRecords
}

func loadConfig() (*config, error) {
	dirname, err := os.UserHomeDir()
	if err != nil {
		log.Fatal(err)
	}
	fd, err := os.Open(dirname + CONFIGPATH + "config.json")
	if err != nil {
		return nil, err
	}

	configFile := []byte{}

	scanner := bufio.NewScanner(fd)
	for scanner.Scan() {
		configFile = append(configFile, scanner.Bytes()...)
	}

	cfg := &config{}
	err = json.Unmarshal(configFile, cfg)
	if err != nil {
		log.Panic("Configuration file could not be parsed as valid json\n")
	}

	err = scanner.Err()

	return cfg, err
}

func getJSON(bytes []byte, container interface{}) error {
	err := json.Unmarshal([]byte(strings.ReplaceAll(string(bytes), "\x00", "")), &container)
	if err != nil {
		log.Println("Failed to unmarshal Njalla response " + err.Error())
		return err
	}
	return nil
}
