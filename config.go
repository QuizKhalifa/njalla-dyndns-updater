package main

const CONFIGPATH string = "/.config/dipd/"
const URL string = "https://njal.la/api/1/"

var CFG *config

type config struct {
	Token   string   `json:"token"`
	Domains []Domain `json:"domains"`
}

type Domain struct {
	TLD        string   `json:"tld"`
	Subdomains []string `json:"subdomains"`
}
