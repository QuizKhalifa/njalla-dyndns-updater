package main

type records struct {
	JsonRPC string `json:"jsonrpc"`
	Result  result `json:"result"`
	ID      string `json:"id"`
}

type result struct {
	Records []recordEntry `json:"records"`
}

type recordEntry struct {
	Content string `json:"content"`
	ID      int    `json:"id"`
	Name    string `json:"name"`
	TTL     int    `json:"ttl"`
	Type    string `json:"type"`
}
