# Dynamic IP Defender

NOTE: Only works with the [Njalla](https://Njal.la) DNS provider for now.

This program runs in the background on a server and continually checks the global IP of itself. The program will send a request to the DNS provider via their API to update the IP if it has changed. If that happens, it should take only up to 5 minutes before the IP is reflected in the DNS. It is therefore recommended to keep the DNS TTL low to ensure rapid propagation.

The program is very useful for self-hosted servers behind a non-business ISP line where you cannot have a static IP or it is an unnecessary cost. In other words, you are "stuck" with a dynamic IP. 

## Build

Requirements:

* Golang
* make

If you want to store the configuration data somewhere else than `$HOME/.config/dipd/config.json`, you need to edit the CONFIGPATH variable in `config.go`.

If you have golang installed on your computer and the target server runs on the same CPU architecture and OS as you (e.g. Windows + amd64), you simply clone the repository and run:

```bash
make build
```

If the server is arm-based (such as a raspberry pi) and running Linux, then simply run:

```bash
make

# or alternatively
make arm
```

In other words, the default architecture target is ARM version 5 for linux.

**NOTE**: The default arm version is 5 (lowest common denominator) (`GOARM=5`), but it can be replaced with both version 6 and 7. The raspberrry pi 4 runs version 8, but golang only supports up to 7.

If you run some other configuration, either edit the makefile or copy the line under arm and edit the environment variables. For windows on an amd64 machine you would do:

```bash
env GOOS=windows GOARCH=amd64 # Notice that GOARM is no longer needed.
```

## Setup on the server (Linux)

When the binary is built, move it to the server and place it somewhere. The program will run under a user, so ideally a separate account should be used.

We'll be using systemd to manage the service. You need to do it the old way if you don't have systemd on your system (check by running `systemctl --version`) (Google it or whatever).

```bash
# We have a user called "user" and the binary is called "dipd"

# Own the binary
sudo chown user:user /path/to/dipd

# Change permissions
chmod 700 /path/to/dipd
```

Now create a file under `/etc/systemd/system/<filename>.server`, for example `/etc/systemd/system/dipd.service`. Then copy the following into the file and edit where necessary:

```bash
[Unit]
Description=Automatically update DNS if IP changes
After=network.target
StartLimitIntervalSec=0
[Service]
Type=simple
Restart=always
RestartSec=1
User=user # Probably want to change this
ExecStart=/home/user/dipd # Probably want to change this

[Install]
WantedBy=multi-user.target
```

---

Now that the service is ready, we need to setup the config file. First create a folder under .config:

```bash
mkdir $HOME/.config/dipd
```

Then add the file `config.json` with the following contents:

```json
{
	"token": "<njalla api token>",
	"domains": [
		{
			"tld": "<The domain you want to monitor>",
			"subdomains": ["<subdomains for that domain. Add a blank one (\"\") if you just want to monitor the 'tld'>"]
		}
	]
}
```

You can get the API token by going to [https://njal.la API Settings](https://njal.la/settings/api/) and click "Add Token". Note that tokens cannot be removed! If you need to revoke a token, click manage and replace the contents of the box under "From:" with 0.0.0.0 and click "EDIT".

You can add multiple domains here by adding more entries into the "domains" array (copy paste the example from line 4 to and including 7 and add a comma at the end of line 7).

---

Now you can start the service with:

```
sudo systemctl enable dipd.service
```

## Check that everything is fine

Do `sudo systemctl status dipd.service` to check if everything is fine. If the config.json has invalid json it will stop. It doesn't complain if the domains are somehow incorrect, so I'd recommend making a manual change to your DNS and check if it works.
